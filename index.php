<?php include("controller.php"); ?>
<!doctype html>
<html lang="en">

<head>
  <title>Aplicación TODO LIST</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <style>
    /* Estilos propios */
    .text-decoration{
      text-decoration-line: line-through;
      color: red;
    }
    .card{
      background-color: #DDDDDD;
      padding: 10px;
      margin-bottom:10px;
    }
    .validate{
      background-color: #BB2D3C;
      color: #FFFFFF;
      padding: 5px;      
    }
    .alert{
      background-color: #167347;
      color: #FFFFFF;
      padding: 5px;
    }
    footer{
      border-top: 1px solid #D1D1D1;
      margin-top:30px;
      padding-top: 20px;
      text-align:center;
    }
  </style>
</head>

<body>
  <h1>TaskList: PHP & MySQL</h1>

  <h3>Lista de tareas</h3>
  
  <form action="index.php" method="post">
    <div>
      <label for="task" >Tarea</label>
      <input type="text" name="task" id="task" placeholder="Escriba su tarea">
      <?php if($error == 1){?>
        <p class="validate">¡El campo Tarea es obligatorio!</p>
      <?php }?>      
    </div>
    <div>
      <label for="description">Descripción</label>
      <br>
      <textarea name="description" id="description" rows="3" placeholder="Escriba una descripción."></textarea>
    </div>
    <input name="add_Task" id="add_Task" type="submit" value="Agregar tarea"> 
  </form>

  <h3>Pendientes:</h3>
  
  <ol>
    <?php
      if($results->rowCount() > 0) {
        foreach ($results as $task){ 
      ?>
        <li class="card">
          <form action="" method="post">
            <a href="?id=<?php echo $task['id']; ?>">[X]</a>
            <input type="hidden" name="filledId" id="task-<?php echo $task['id']; ?>" value="<?php echo $task['id']; ?>" >
            <input type="checkbox" name="task_check" value="<?php echo $task['status']; ?>" onChange="this.form.submit()" <?php echo ($task['status'] == 1)?'checked':''; ?>>
            <label class="<?php echo ($task['status'] == 1)?'text-decoration':''; ?>"><?php echo $task['task']; ?></label>            
          </form>
          <p>
            <i><?php echo $task['created_at']; ?></i>
            <div>
              <?php echo $task['description']; ?>
            </div>
          </p>
        </li>    
      <?php
        }
      }else {
        echo "<p class='alert'>¡No hay tareas que mostrar!</p>";
      }
      // Cerrar la conexión PDO
      $conn = null;
    ?>
  </ol>

  <footer>
    CUCEA / TECHFEST© 2023 BY AFFL
  </footer>

</body>
</html>