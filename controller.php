<?php
    include("connection.php");
    
    // Redirección
    function redirect($page){
        header("Location: $page");
    }

    // Registrar la actividad
    if(isset($_POST['add_Task'])){
        if(empty($_POST['task'])){
            $error = 1;
        } else {
            $task = $_POST['task'];
            $description = $_POST['description'];

            // Insertar la información a la base de datos
            $sql = "INSERT INTO tasks (task, description) VALUE (:task, :description)";
            $sql = $conn->prepare($sql);
            $sql->execute(['task'=> $task, 'description' => $description]);
            
            redirect("/tasklist2");
        }
    }
 
    // Cambiar el estatus de la actividad
    if(isset($_POST['filledId'])){
        $filledId = $_POST['filledId'];
        $task_check = (isset($_POST['task_check']))?1:0;
        
        $sql = "UPDATE tasks SET status = :task_check WHERE id = :filledId";
        $sql = $conn->prepare($sql);
        $sql->execute(['task_check'=> $task_check, 'filledId' => $filledId]);
        
        redirect("/tasklist2");
    }

    // Eliminar actividad
    if(isset($_GET['id'])){
        $task = $_GET['id'];
        
        // Eliminar la información a la base de datos
        $sql = "DELETE FROM tasks WHERE id = '$task'";
        $sql = $conn->prepare($sql);
        $sql->execute();

        redirect("/tasklist2");
    }

    // Mostrar listado completo de las actividades
    $sql = "SELECT * FROM tasks ORDER BY status, created_at DESC";
    $results = $conn->query($sql);
