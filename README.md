# TaskList v1.0

Pequeña aplicación web que tiene como propósito el organizar y gestionar tareas pendientes.
 
### Estructura

- HTML 5
- CSS 3
- PHP
- MySQL

### Integración

- Bootstrap 5.2

### Pasos a seguir

1. Instalación de 
    [WAMPServer](https://www.wampserver.com/en/)

2. Una vez que la instalación haya concluido correctamente, deberá correr el servidor(WAMP) para iniciar los servicios.

3. Creación de la base de datos a través de MySQL a través de la herramienta phpMyAdmin:

    - Abra su navegador web e ingrese a la dirección localhost/phpmyadmin (Recuerde haber previamente iniciado el servidor).
    - Ingrese nombre de usuario y contraseña, ejemplo: root.
    - Para no crear la base de datos desde 0, solo cree una vacía e importe el archivo "tasklist" que se encuentra en este repositorio.
    - ¡Listo! su base de datos se ha creado.

4. Copiar el contenido del repositorio en una carpeta (**tasklist** por ejemplo) en la ubicación de WAMP.
**Ejemplo**
`C:\wamp\www\`

5. Por último, a través del navegador web, accederá a su servidor local con "**localhost**" ó "**127.0.0.1**", seguido de una **/** y el _nombre de la carpeta_ en donde se copió el contenido de este repositorio.

¡Listo! ahora podrás utilizar la aplicación.
