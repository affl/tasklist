<?php
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $database = "tasklist";

    try{
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }catch(PDOException $e){
        echo "Existe un error en la conexión de la base de datos: " . $e->getMessage(). "</p>";
    }
?>